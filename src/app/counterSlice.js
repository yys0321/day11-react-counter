import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    size: 0,
    sum: 0,
}

let config = {
    name: "Counter",
    initialState,
    reducers: {
        updateSize: (state, action) => {
            state.size = action.payload;
        },
        updateSum: (state, action) => {
            state.sum = action.payload
        }
    }
}

const counterSlice = createSlice(config)

export const { updateSize, updateSum } = counterSlice.actions
export default counterSlice.reducer;
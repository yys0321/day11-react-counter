import MultipleCounter from './components/MultipleCounter';

function App() {
  return <MultipleCounter />;
}

export default App;
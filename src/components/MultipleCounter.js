import React, { useEffect, useState } from 'react';
import './MultipleCounter.css';
import CounterSizeGenerator from './CounterSizeGenerator';
import CounterGroupSum from './CounterGroupSum';
import CounterGroup from './CounterGroup';
import { useDispatch } from "react-redux";
import { useSelector } from 'react-redux';
import { updateSize, updateSum } from '../app/counterSlice';

const MultipleCounter = () => {
    const sizeFromStore = useSelector((state) => state.counter.size);
    const sumFromStore = useSelector((state) => state.counter.sum);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(updateSum(0));
    }, [sizeFromStore]);

    const handleSize = (size) => {
        dispatch(updateSize(size));
    };

    const handleSum = (sum) => {
        dispatch(updateSum(sum));
    };

    return (
        <div className='container'>
            <div>
                <CounterSizeGenerator size={sizeFromStore} setSize={handleSize} />
                <CounterGroupSum sum={sumFromStore} />
                <CounterGroup size={sizeFromStore} setSum={handleSum} />
            </div>
        </div>
    );
};

export default MultipleCounter;
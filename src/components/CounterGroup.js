import React from 'react';
import Counter from './Counter';
import './CounterGroup.css';
import { useSelector } from 'react-redux';

const CounterGroup = ({ }) => {
  const sizeFromStore = useSelector((state) => state.counter.size);

  return (
    <div className='group-container'>
      {sizeFromStore ? Array(parseInt(sizeFromStore)).fill(0).map((_, index) =>
        <Counter key={index} id={index} />) : null}
    </div>
  );
};

export default CounterGroup;
import React from "react";
import { useSelector } from 'react-redux';

const CounterGroupSum = ({ }) => {
    const sumFromStore = useSelector((state) => state.counter.sum)

    return (
        <div>Sum: {sumFromStore}</div>
    );
};

export default CounterGroupSum;
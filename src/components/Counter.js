import React, { useEffect, useState } from 'react';
import './Counter.css';
import { useDispatch, useSelector } from 'react-redux';
import { updateSum } from '../app/counterSlice';


const Counter = ({ }) => {
    const [count, setCount] = useState(0);
    const sizeFromStore = useSelector((state) => state.counter.size);
    const sumFromStore = useSelector((state) => state.counter.sum);
    const dispatch = useDispatch();

    const addCount = () => {
        setCount((previous) => previous + 1);
        dispatch(updateSum(sumFromStore + 1))
    };
    const minusCount = () => {
        setCount((previous) => previous - 1);
        dispatch(updateSum(sumFromStore - 1))
    };

    useEffect(() => {
        setCount(0);
    }, [sizeFromStore]);

    return (
        <div className='counter-container'>
            <button onClick={addCount}>+</button>
            <div className='counter-count'>{count}</div>
            <button onClick={minusCount}>-</button>
        </div>
    );
};

export default Counter;
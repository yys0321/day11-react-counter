import React from "react";
import "./CounterSizeGenerator.css";
import { useDispatch, useSelector } from "react-redux";
import { updateSize } from "../app/counterSlice";

function CounterSizeGenerator() {
    const sizeFromStore = useSelector((state) => state.counter.size)
    const dispatch = useDispatch();

    const handleChange = (event) => {
        const inputValue = event.target.value
        const inputSize = inputValue === "" ? 0 : parseInt(inputValue)

        dispatch(updateSize(inputSize))
    }

    return (
        <div className="size-counter">
            <div>size:</div>
            <input className="size-input" type="number" min={0} onChange={handleChange} />
        </div>
    );
};

export default CounterSizeGenerator;